@extends('layout.layout-lagi')
@section('content')
    <h2>Tambah Peliharaan</h2>
    <form method="post" action="{{ url('pet/store') }}">
        @csrf
        <div>
            <label for="">Nama Peliharaan : </label> <br />
            <input type="text" name="nama">
        </div>
        <br />

        <div>
            <label for="">Jenis Peliharaan : </label> <br />
            <input type="text" name="jenis">
        </div>
        <br />

        <div>
            <label for="">Pemilik Peliharaan :</label> <br />
            <select name="owner">
                <option value="" selected disabled>Pilih Owner</option>
                @foreach ($owners as $owner)
                    <option value="{{ $owner->id }}">{{ $owner->nama }}</option>
                @endforeach
            </select>
        </div>
        <br />

        <button>Tambah</button>
    </form>
@endsection