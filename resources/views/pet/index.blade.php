<p>
    <a href="{{ url('pet/create') }}">Tambah Peliharaan</a>
</p>

<table style="width:100%" border="1">
    <tr>
        <th>Nama</th>
        <th>Jenis</th>
        <th>Pemilik</th>
        <th></th>
    </tr>
    @foreach ($pets as $pet)
        <tr>
            <td>{{ $pet->pet_name }}</td>
            <td>{{ $pet->pet_type }}</td>
            <td>{{ $pet->pet_owner }}</td>
            <td>
                <a target="_blank" href="{{ url('pet/edit/' . $pet->id) }}">edit</a>
                <a href="{{ url('pet/delete/' . $pet->id) }}">delete</a>
            </td>
        </tr>
    @endforeach
</table>