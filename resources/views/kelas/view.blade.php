@extends('layout.layout-baru') 
@section('title', 'View Kelas ' . $kelas->nama_kelas) 
@section('content')
<div class="row">
    <div class="col-md-12">
        <form>
            <div class="form-group">
                <label for="">Nama Kelas</label>
                <input disabled type="text" class="form-control" value="{{ $kelas->nama_kelas }}">
            </div>
            <div class="form-group">
                <label for="">Kode Kelas</label>
                <input disabled type="text" class="form-control" value="{{ $kelas->kode_kelas }}">
            </div>
        </form>
    </div>
</div>
@endsection