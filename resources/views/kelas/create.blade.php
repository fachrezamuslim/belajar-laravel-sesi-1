@extends('layout.layout-baru') 
@section('title', 'Create Kelas') 
@section('content')
<div class="row">
    <div class="col-md-12">
        <form method="post" action="{{ url('kelas/add') }}">
            @csrf
            <div class="form-group">
                <label for="">Nama Kelas</label>
                <input type="text" class="form-control" name="nama">
            </div>
            <div class="form-group">
                <label for="">Kode Kelas</label>
                <input type="text" class="form-control" name="kode">
            </div>
            <button class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection