@extends('layout.layout-app') 
@section('title', 'Tambah Jurusan') 
@section('content')
<h1>Tambah Jurusan</h1>
<form action="{{ url('jurusan/add') }}" method="post">
    @csrf
    <div class="form-group">
        <label for="">Nama Jurusan</label>
        <input type="text" class="form-control" name="nama">
    </div>
    <div class="form-group">
        <label for="">Kode Jurusan</label>
        <input type="text" class="form-control" name="kode">
    </div>
    <div class="form-group">
        <label for="">Deskripsi</label>
        <textarea name="deskripsi" class="form-control"></textarea>
    </div>
    <button class="btn btn-primary">Submit</button>
</form>
@endsection