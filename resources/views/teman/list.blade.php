<p>
    <a href="{{ url('teman/create') }}">Tambah Teman</a>
</p>

<table style="width:100%" border="1">
    <tr>
        <th>Nama</th>
        <th>Deskripsi</th>
        <th></th>
    </tr>
    @foreach ($teman as $item)
        <tr>
            <td>{{ $item->nama }}</td>
            <td>{{ $item->deskripsi }}</td>
            <td>
                <a target="_blank" href="{{ url('teman/edit/' . $item->id) }}">edit</a>
                <a href="{{ url('teman/delete/' . $item->id) }}">delete</a>
            </td>
        </tr>
    @endforeach
</table>