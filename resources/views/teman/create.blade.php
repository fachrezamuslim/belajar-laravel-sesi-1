@extends('layout.layout-lagi')
@section('content')
    <h2>Tambah Teman</h2>
    <form method="post" action="{{ url('teman/store') }}">
        @csrf
        {{ csrf_field() }}
        <div>
            <label for="">Nama : </label> <br />
            <input type="text" name="nama">
        </div>
        <br />
        <div>
            <label for="">Deskripsi : </label> <br />
            <textarea name="deskripsi"></textarea>
        </div>

        <button>Tambah</button>
    </form>
@endsection