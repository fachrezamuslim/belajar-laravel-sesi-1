@extends('layout.layout-lagi')
@section('content')
    <h2>Edit Teman</h2>
    <form method="post" action="{{ url('teman/update') }}">
        @csrf
        <input name="id" type="hidden" value="{{ $teman->id }}">
        <div>
            <label for="">Nama : </label> <br />
            <input value="{{ $teman->nama }}" type="text" name="nama">
        </div>
        <br />
        <div>
            <label for="">Deskripsi : </label> <br />
            <textarea name="deskripsi">{{ $teman->deskripsi }}</textarea>
        </div>

        <button>Update</button>
    </form>
@endsection