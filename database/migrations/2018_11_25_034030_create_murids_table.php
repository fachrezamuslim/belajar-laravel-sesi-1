<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMuridsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('murids', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jurusan_id')->unsigned();
            $table->integer('guru_id')->unsigned();
            $table->string('nama_murid');
            $table->text('alamat');
            $table->timestamps();

            $table->foreign('jurusan_id')->references('id')->on('jurusans');
            $table->foreign('guru_id')->references('id')->on('gurus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('murids');
    }
}
