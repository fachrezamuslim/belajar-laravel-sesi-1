<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'user'], function () {
    Route::get('/', function(){
        return "Halaman User";
    });

    Route::post('/add', function(){
        return "Tambah User";
    });

    Route::get('/{id}/get', function($id){
        return $id;
    });
});

Route::group(['prefix' => 'jurusan'], function () {
    Route::get('/', 'JurusanController@daftar');
    Route::get('/create', 'JurusanController@create');
    Route::get('/{id}/find', 'JurusanController@find');
    Route::get('/{id}/edit', 'JurusanController@edit');
    Route::get('/{id}/delete', 'JurusanController@delete');

    Route::post('add', 'JurusanController@add');
    Route::post('update', 'JurusanController@update');
});

Route::group(['prefix' => 'kelas'], function () {    
    Route::get('/create', 'KelasController@create');
    Route::post('/add', 'KelasController@add');

    Route::get('/{kelas_id}/view/', 'KelasController@view');
});

Route::get('teman/create', 'TemanController@create');
Route::post('teman/store', 'TemanController@store');
Route::get('teman/list', 'TemanController@list');
Route::get('teman/edit/{id}', 'TemanController@edit');
Route::get('teman/delete/{id}', 'TemanController@delete');
Route::post('teman/update', 'TemanController@update');

Route::group(['prefix' => 'pet'], function () {
    Route::get('create', 'PetController@create');
    Route::post('store', 'PetController@store');
    Route::get('/', 'PetController@index');
});