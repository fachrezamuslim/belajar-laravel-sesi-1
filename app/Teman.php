<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teman extends Model
{
    protected $guarded = [];

    function pets() {
        return $this->hasMany('App\Pet', 'pet_owner', 'id');
    }
}
