<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jurusan;

class JurusanController extends Controller
{
    function daftar(){
        // SELECT * FROM jurusans
        // $data = Jurusan::all();

        // dd($data);
        // return "controller daftar jurusan";
        return view('jurusan.index');
    }

    function create(){
        return view('jurusan.create');
    }

    function add(Request $request){
        // Melihat Request
        // dd($request->all());

        // Cara 1
        // $create = new Jurusan;
        // $create->nama_jurusan = $request->nama;
        // $create->kode_jurusan = $request->kode;
        // $create->deskripsi = $request->deskripsi;
        // $create->save();

        // Cara 2
        // $create = Jurusan::create([
        //     'nama_jurusan' => $request->nama_jurusan,
        //     'kode_jurusan' => $request->kode_jurusan,
        //     'deskripsi' => $request->deskripsi,
        // ]);

        return "Berhasil";
    }

    function find($id){
        // SELECT * FROM jurusans WHERE id = $id
        $data = Jurusan::where('id', $id)->first();

        dd($data);

        // return "mencari jurusan dengan id : " . $id;
    }
}