<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teman;

class TemanController extends Controller
{
    function create(){
        return view('teman.create');
    }

    function store(Request $request){
        // Input
        $dataToCreate = [
            'nama'      => $request->nama,
            'deskripsi' => $request->deskripsi
        ];

        // Proses
        $created = Teman::create($dataToCreate);

        // Output
        return redirect('/teman/list');
    }

    function list(){
        $data['teman'] = Teman::with('pets')->get();
        $data['tanggal'] = "hari ini";
        dd($data);
        return view('teman.list', $data);
    }

    function edit($id){
        $data['teman'] = Teman::where('id', $id)->first();

        return view('teman.edit', $data);
    }

    function update(Request $request){
        // dd($request->all());

        $update = Teman::where('id', $request->id)->first();
        $update->nama = $request->nama;
        $update->deskripsi = $request->deskripsi;
        $update->save();

        return redirect('/teman/list');
    }

    function delete($id){
        $delete = Teman::where('id', $id)->first();
        $delete->delete();

        return redirect('teman/list');
    }
}
