<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kelas;

class KelasController extends Controller
{
    function create(){
        return view('kelas.create');
    }

    function add(Request $request){
        // dd($request->all());

        $create = Kelas::create([
            'nama_kelas'    => $request->nama,
            'kode_kelas'    => $request->kode
        ]);

        return redirect()->back()->with('pesan', 'Berhasil menambahkan kelas');
    }

    function view($kelas_id){
        // dd($kelas_id);
        $data['kelas'] = Kelas::where('id', $kelas_id)->first();
        return view('kelas.view', $data);
    }
}
