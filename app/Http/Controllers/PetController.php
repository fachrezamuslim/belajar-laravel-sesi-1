<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teman;
use App\Pet;

class PetController extends Controller
{
    function index() {
        // $data['pets'] = Pet::with('owner')->get();
        $data['pets'] = \DB::select('SELECT p.pet_type, p.pet_name, p.id, o.nama AS pet_owner FROM pets AS p INNER JOIN temen AS o ON p.pet_owner = o.id');

        // dd($data['pets']);
        return view('pet.index', $data);
    }

    function create () {
        // mengambil data owner dari table teman
        $data['owners'] = Teman::all();

        return view('pet.create', $data);
    }

    function store(Request $request) {
        // dd($request->all());
        $create = Pet::create([
            'pet_name'      => $request->nama,
            'pet_type'      => $request->jenis,
            'pet_owner'     => $request->owner
        ]);

        return "Berhasil";
    }
}
