<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    protected $guarded = [];

    function owner () {
        return $this->belongsTo('App\Teman', 'pet_owner', 'id');
    }
}
